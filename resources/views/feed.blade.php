
<?=
'<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL
?>
<rss version="2.0">
    <channel>
    <title>The guardian</title>
    <link>{{$newsData->section->webUrl}}</link>
    <description>Guardian rss feed</description>
    <language>en-us</language>

        @foreach($newsData->results as $news)
            <item>
                <title>{{ $news->webTitle }}</title>
                <link>{{ $news->webUrl }}</link>
                <description>{{ $news->webTitle }}</description>
                <category>{{ $news->pillarId }}</category>
                <guid>{{ $news->id }}</guid>
                <pubDate>{{ date("D, d M Y H:i:s T", strtotime($news->webPublicationDate)) }}</pubDate>
            </item>
        @endforeach
    </channel>
</rss>