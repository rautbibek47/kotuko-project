<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(Request $request){
        try{
            $news = cache()->remember($request->section,600,function(){    
                $response= Http::get(config('app.api_url').request('section'),[
                    'api-key'=>config('app.api_key'),
                ]);
                return json_decode($response);
            });
            
            if($news->response->status=='error'){
                cache()->forget($request->section);
                return response()->json([
                    'message'=>'The requested resource could not be found.',
                ],404);
            }
            $newsData= $news->response;
           
            return response()->view('feed', compact('newsData'))->header('Content-Type', 'application/xml');
           
        }catch(\Exception $e){
            return response()->json([
                'error'=>$e->getMessage(),
                'code' =>$e->getCode(),
            ]);
        }        
    }
}
